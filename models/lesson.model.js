const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const Schema = mongoose.Schema;

let lessonSchema = new Schema({
    name: String,
});


module.exports = mongoose.model("lesson", lessonSchema);
