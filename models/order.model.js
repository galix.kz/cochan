const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const Schema = mongoose.Schema;

const ORDER_STATUSES = {
    WAITING_TO_ACCEPT: 'waiting_to_accept',
    WAITING_FOR_PAYMENT: 'waiting_for_payment',
    PAID: 'paid',
    FINISHED: 'finished'
}
let orderSchema = new Schema({
    teacherId: {
        type: Schema.Types.ObjectID,
        ref: 'user'
    },
    studentId: {
        type: Schema.Types.ObjectID,
        ref: 'user'
    },
    lessonId: {
        type: Schema.Types.ObjectID,
        ref: 'lesson'
    },
    price: Number,
    status: {
       type: String,
       enum: ['waiting_to_accept', "waiting_for_payment", "paid",  "finished"],
       default: ORDER_STATUSES.WAITING_TO_ACCEPT
    },
    conferenceId: String,
});


module.exports = mongoose.model("order", orderSchema);
module.exports.ORDER_STATUSES = ORDER_STATUSES
