const express = require("express");
const app = express();
const port = process.env.PORT || 8080;
const mongoose = require("mongoose");
const passport = require("passport");
const flash = require("connect-flash");
const morgan = require("morgan");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const session = require("express-session");
const path = require("path");
const server = require('http').Server(app);
const io = require('socket.io')(server);
const { ExpressPeerServer } = require('peer');
const MongoStore = require('connect-mongo')(session);

const peerServer = ExpressPeerServer(server, {
  debug: true
});



const { v4: uuidV4 } = require('uuid')

app.use('/peerjs', peerServer);



const User = require("./models/user.model");
const dbConfig = require("./config/database.config");

// Configuration
mongoose.connect(dbConfig.url, {
  useNewUrlParser: true
});
require("./config/passport.config")(passport);

// app.use(session({
//   secret:'margherita',
//   maxAge: new Date(Date.now() + 3600000),
//   store: new MongoStore(
//       // Following lines of code doesn't work
//       // with the connect-mongo version 1.2.1(2016-06-20).
//       //    {db:mongoose.connection.db},
//       //    function(err){
//       //        console.log(err || 'connect-mongodb setup ok');
//       //   }
//       {mongooseConnection:mongoose.connection}
//   )
// }));


// Express setup
app.use(morgan("dev"));
app.use(cookieParser());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));
app.use(express.static(__dirname + "/public"));

// Passport setup
app.use(session({
  secret: "margherita",
  resave: false,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// Routes
require("./routes/routes")(app, passport, User);
require("./routes/room")(app, io, uuidV4);
// Launch server
server.listen(port, () => console.log(`server started on port ${port}`));
