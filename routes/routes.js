const Lesson = require('../models/lesson.model')
const Order = require('../models/order.model')
const User = require('../models/user.model')
const { ORDER_STATUSES } = require('../models/order.model')
const CryptoJs = require('crypto-js')
const { v4: uuidV4 } = require('uuid')
module.exports = (app, passport, UserModel) => {

  // Home Page
  app.get("/", (req, res) => res.render("home", {
    isAuth: req.isAuthenticated(),
    user: req.user
  }));
  app.post("/lesson-add",  (req, res) => {

        const lesson = new Lesson({name: req.body.name})
        lesson.save(function (err, data) {
          if (err) {
            console.log(err);
          } else {
            return res.redirect('/profile')
          }
        })
      }
  )
  app.post("/lesson-select", isLoggedIn, async (req, res) => {
      console.log(req.body)
        const teacher = await User.findOne({_id: req.body.teacherId})
        console.log('teacher', teacher)
        await teacher.update({lessonId: req.body.lessonId})
        teacher.save()

        return res.redirect('/profile')
      }
  )
  app.post("/order-create", isLoggedIn, async (req, res) => {
        const {lessonId, price} = req.body;
        let order = await Order.findOne({studentId: req.user._id})
        console.log('order sad', order)
        if (order!==null) {
          await Order.update({lessonId: lessonId, studentId: req.user._id, price: price, conferenceId: null, teacherId: null, status: ORDER_STATUSES.WAITING_TO_ACCEPT})
          order.save()
        } else {
          order = await new Order({lessonId: lessonId, price: price, studentId: req.user._id, status: ORDER_STATUSES.WAITING_TO_ACCEPT})
          order.save()
        }
        return res.redirect('/profile')
      }
  )
    app.post("/order-accepted", isLoggedIn, async (req, res) => {
        console.log(req.body)
            const {lessonId} = req.body;
            let order = await Order.findOne({lessonId: lessonId})

            if (order!==null) {
                await order.update({teacherId: req.user._id, status: ORDER_STATUSES.WAITING_FOR_PAYMENT})
                order.save()
            }
            return res.redirect('/profile')
        }
    )
    app.get("/order-payment/:id", isLoggedIn, async (req, res) => {
            console.log(req.body)
            let order = await Order.findOne({_id: req.params.id})

            if (order!==null) {
                const salt = Math.random().toString(36).substring(7);
                // console.log('id', subscribe.id)
                let datas = {
                    pg_merchant_id: 515533,
                    pg_amount: order.price,
                    pg_description: `Покупка на Kochan`,
                    pg_salt: salt,
                    pg_testing_mode: 0,
                    pg_success_url: `http://${req.get('host')}/order-success/${order._id}`,
                }
                let hashString = '';
                let urlString = "https://api.paybox.money/payment.php?"
                const sortObject = o => Object.keys(o).sort().reduce((r, k) => (r[k] = o[k], r), {})
                datas = sortObject(datas)
                for (let data in datas) {
                    hashString += datas[data] + ';'
                    urlString += `${data}=${datas[data]}&`
                }
                hashString = 'payment.php;' + hashString + 'WSQB7H64BLpBORzX';
                hashString = CryptoJs.MD5(hashString).toString()
                urlString += `pg_sig=${hashString}`
                // console.log(subscribe.id)

                return res.redirect(urlString);

            }
            return res.redirect('/profile')
        }
    )
  app.get("/order-success/:id", async (req, res) => {
        console.log(req.body)
      console.log(req.params)
        const fullUrl = 'https://' + req.get('host') + '/room/' + uuidV4();
        const {lesson} = req.body;
        let order = await Order.findOne({_id: req.params.id})
        console.log(order)
        if (order!==null) {
            if(order.teacherId) {
                const teacher = await UserModel.findOne({_id: order.teacherId})
                console.log('th', teacher)
                await teacher.update({balance: teacher.balance+order.price})
                teacher.save()
            }
            await order.update({conferenceId: fullUrl, status: ORDER_STATUSES.PAID})
          order.save()
        }
        return res.redirect('/profile')
      }
  )
  // Login
  app.get("/login", (req, res) => res.render("login", {
    message: req.flash("loginMessage"),
    isAuth: req.isAuthenticated(),
    user: req.user
  }));

  app.post("/login", passport.authenticate("local-login", {
    successRedirect: "/profile", // redirect to the secure profile section
    failureRedirect: "/login", // redirect back to the signup page if there is an error
    failureFlash: true // allow flash messages
  }));

  // Signup
  app.get("/signup", (req, res) => res.render("signup", {
    isAuth: req.isAuthenticated(),
    user: req.user
  }));
    app.get("/signup-admin", (req, res) => res.render("signup-admin", {
        isAuth: req.isAuthenticated(),
        user: req.user
    }));
  app.post("/signup", passport.authenticate("local-signup", {
    successRedirect: "/profile",
    failureRedirect: "/signup"
  }));

  // Profile
  app.get("/profile", isLoggedIn, (req, res) => {

    let redirectUrl = `/user/${req.user.username}`;
    res.redirect(redirectUrl);
  });

  app.get("/user/:username", (req, res) => {
    let username = req.params.username;
    UserModel.findOne({
      username
    }).populate('lessonId').exec(async (err, doc) => {
      if (err) throw err;
      if (!doc)
        res.render("404", {
          isAuth: req.isAuthenticated(),
          user: req.user,
          profile: null,
        });
      else {
        const lessons =  await Lesson.find({});
        let lesson = null;
        let order = null;
        let orderedLesson = null
        let orders = null

          let teachers = null
        if(doc.isTeacher){
          if(doc.lessonId) orders = await Order.find({lessonId: doc.lessonId._id}).populate('lessonId')
            console.log('teachers', orders)
        }
        if(doc.isAdmin){
            teachers = await UserModel.find({isTeacher: true}).populate('lessonId')

        }
        else{
          order = await Order.findOne({studentId: req.user._id}).populate('lessonId')

        }
        console.log('lesson', order)
        res.render("profile", {
          lessons: lessons,
          isAuth: req.isAuthenticated(),
          lesson: lesson,
          user: req.user,
          profile: doc,
          order: order,
          orders: orders,
            teachers: teachers,
          isRoot: req.isAuthenticated() ? doc.username === req.user.username : false
        });
      }
    });

  });
  app.get("/logout", (req, res) => {
    req.logout();
    res.redirect("/");
  });

  app.get("/check-username-availability", (req, res) => {
    UserModel.find((err, users) => {
      if (err) throw err;
      let usernames = users.map(val => val.username);
      res.json(usernames);
    });
  });

  // About Page
  app.get("/about", (req, res) => {
    res.render("about", {
      isAuth: req.isAuthenticated(),
      user: req.user,
    });
  });

  // app.get("*", (req, res) => {
  //   res.render("404", {
  //     isAuth: req.isAuthenticated(),
  //     user: req.user,
  //   });
  // });
};

function isLoggedIn(req, res, next) {
  // if user is authenticated in the session, carry on
  if (req.isAuthenticated())
    return next();
  // if they aren't redirect them to the home page
  res.redirect("/");
}
