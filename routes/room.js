module.exports = (app, io, uuidV4) => {
    app.get('/room/', (req, res) => {
        res.redirect(`/room/${uuidV4()}`)
    })


    app.get('/room/:room', (req, res) => {
        res.render('room', { roomId: req.params.room })
    })

    io.on('connection', socket => {
        socket.on('join-room', (roomId, userId) => {
            socket.join(roomId)
            socket.to(roomId).broadcast.emit('user-connected', userId);
            // messages
            socket.on('message', (message) => {
                //send message to the same room
                io.to(roomId).emit('createMessage', message)
            });

            socket.on('disconnect', () => {
                socket.to(roomId).broadcast.emit('user-disconnected', userId)
            })
        })
    })
}
